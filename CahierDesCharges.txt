�nonc� :

Tu devras r�aliser une version mobile de Temple Run.
Au d�marrage, un menu contenant un bouton pour jouer et un bouton pour voir les r�gles ainsi que le meilleur score est affich�.
Tu es invit� � consulter l�application Temple Run pour plus de d�tails.

Les fonctionnalit�s attendues sont les suivantes:

    G�n�ration d�un chemin avec virages � 90�
    Obstacles
    Saut
    R�cup�ration de pi�ces


	L�utilisation du gyroscope afin de te d�placer sur le chemin courant est un BONUS et n�est pas attendu dans le rendu standard. Tu peux, par contre, te faire plaisir et nous montrer tes comp�tences en ajoutant des bonus ou de nouvelles fonctionnalit�s.

	L'interface graphique n'a pas besoin d'�tre �labor�e, il suffit que le jeu soit jouable. Pour l'�valuer, nous nous concentrerons sur l'organisation de ton code, de la/des scene(s) ainsi que ta capacit� � �voluer dans un langage orient� objet.
	Il n'y a pas de deadline pour rendre ce projet, tu le rends quand tu as fini. Un simple README devra r�sumer le fonctionnement de l'application, la proc�dure d'installation, les points � am�liorer et le temps pass� sur le d�veloppement.
	Tu publieras ton code source sur GitHub et nous enverras un lien afin que l'on puisse le lire et le tester. Tu nous fera �galement parvenir un fichier .APK (Android) et/ou .IPA (iOS) afin que nous puissions tester simplement ton jeu.