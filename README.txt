Cette application a �t� faite par R�mi Sanchez pour l'entreprise MadJoh en tant que test d'entr�e en alternance:

Fonctionnement de l'application:
Temple run like, lire les r�gles dans le Menu Principal.

Installation de l'application:
D�placer l'apk sur votre t�l�phone, cliquer dessus et l'installer.
Vous devrez accepter l'autorisation d'acc�s au stockage car les sauvegardes se font sur la carte SD.
N'ayant pas de mac ni de iphone, je ne pr�f�re pas m'avancer sur une version Iphone, en th�orie �a devrait fonctionner avec la bonne compilation!

Structure du code:
Je suis rest� assez simple sur la structure du code vu que l'application n'est pas tr�s grosse et ne comporte pas �norm�ment d'entit� diff�rente.
Mais nous avons tout de m�me une structure 3-tier (client, m�tier, physique) et MVC (mod�le vue controller) avec l'interface UI d'unity pour la vue,
la couche client regroupant mes controllers pour controller, la couche m�tier avec mes entit�s et enfin la couche physique pour mes donn�es persistances/script de sauvegarde.
Nous avons avec cela du design patern factory/singleton pour travailler avec des objets static durant une partie.

Points � am�liorer:
De pr�f�rence, � lire apr�s avoir test� l'application.
Les sauvegardes, si possible les avoir sur une base de donn�es, et si c'est une application google play, utiliser les comptes google + pour cela
Je ne me suis pas renseign� � ce sujet, mais cela a l'air d'�tre fait assez facilement. Ici nous n'enregistrons qu'une seule entit�, j'aurai pu la diviser,
mais au vu du peu de chose � enregistrer et voulant rester assez simpliste, j'ai tout r�uni dans une entit� "Save".
La correction de quelques bugs, comme l'emplacement des bonus sur les ponts. La glissade qui ne marche pas toujours, je n'ai pas r�ussi � trouver le probl�me, m�me si
je pense que c'est un probl�me de temps de traitement.
Ajout d'autres bonus et pouvoir.
Ajout d'un syst�me de succ�s, assez facile � rajouter, il suffit de rajouter des booleans dans notre entit� Save et d'avoir un SuccesController en fin de chaque partie
pour voir si un succ�s est remport� : je n'ai pas voulu le faire pour notre petite application ahah.
Am�liorer les graphismes!! Forc�ment ici j'ai utilis� des mod�les 3D que j'ai trouv� sur l'asset store, je pense qu'il faudrait des personnages d�j� moins pouss� niveau graphisme.
Avoir un environnement 3D coh�rent ahah.
La glissade de SapphiChan n'existait pas � la base, j'ai modifi� ses animations pour en cr�er une... c'est pour �a qu'elle n'est pas jolie-jolie!
Et enfin, nous ne tournons pas r�ellement � 90�, c'est la map qui tourne... Pour g�rer de vrais virages � 90� (je ne sais pas si TempleRun l'a vraiment fait mais) il faut
constamment v�rifier si notre personnage court dans telle direction, gr�ce � �a nous pourrons le faire avancer non plus que sur l'axe x, mais aussi sur z+,z- et x-, donc 3 axe en plus,
en plus de �a, il faudrait que le terrain se g�n�re selon sa direction et donc aussi sur les 4 axes au lieu d'un seul.
J'ai trouv� �a plus lourd � faire et je suis donc rest� sur un seul axe, malgr� que le rendu des virages n'est qu'une illusion imparfaite.

Temps pass�:
J'ai pass� 36 heures sur ce projet, soit 3,5 jours pour 10-12 heures par jour et 4 le dernier.
-J'ai � peu pr�s passer 10h � me document et sur des recherches internet. J'avais trouv� un tuto assez similaire � ce que je devais faire, mais il �tait enfaite tr�s mal fait et
tr�s incomplet, il m'a fait perdre �norm�ment de temps sur ma programmation, heureusement, j'ai su tout de m�me en tirer du bien.
-8 h � coder et r�fl�chir � la structure de mon projet sans soucis, sans interruption, 8h de programmation pur en gros.
-12h � corriger les bugs et � changer mon code mainte et mainte fois.
-Et pour finir, 4 heures � tester l'application, et la faire tester � des connaissances pour r�cup�rer les "bug report" des autres joueurs, ainsi que leur avis!
Je code actuellement aussi sur un pc pas �norm�ment performant, et je pense perdre pas mal de temps � cause de cela.

Ps: J'esp�re que cette application vous aura bien fait rire et que la structure du code vous pla�t!
    Mais surtout, je peux m'adapter, il me reste plein de chose � apprendre (sachant que je n'ai pas encore fait de "vrais cours" en Unity, et si vous avez une fa�on de structurer un code
    et autre conseil, je suis preneur!

