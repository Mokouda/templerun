﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Assets.Scripts.Metier;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private GameObject PanelGameOver;
    [SerializeField]
    private GameObject PanelPause;

    private float transition = 0.0f;

    // Update is called once per frame
    void Update()
    {
        GameObject.Find("Canvas/TextScore").GetComponent<Text>().text = "Score : " + MetierFactory.Game.score;
        GameObject.Find("Canvas/PanelRing/TextRing").GetComponent<Text>().text = MetierFactory.Game.rings.ToString();
        if (PanelGameOver.activeInHierarchy)
        {
            transition += Time.deltaTime;
            PanelGameOver.GetComponent<Image>().color = Color.Lerp(new Color(0, 0, 0, 0), Color.black, transition);
            GameObject.Find("Canvas/PanelGameOver/Image/CharacterNo").transform.Rotate(0, 1, 0, Space.Self);
        }
        if (Input.GetKeyDown("escape"))
        {
            showPauseMenu();
        }
    }

    public void TryAgain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void showPauseMenu()
    {
        PanelPause.SetActive(true);
        Time.timeScale = 0;
    }

    public void Continue()
    {
        StartCoroutine(waitToContinue());
        PanelPause.SetActive(false);
    }

    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public IEnumerator waitToContinue()
    {
        Time.timeScale = 0.02f;
        GameObject.Find("Canvas/TextCount").GetComponent<Text>().text = "3";
        yield return new WaitForSecondsRealtime(1);
        GameObject.Find("Canvas/TextCount").GetComponent<Text>().text = "2";
        yield return new WaitForSecondsRealtime(1);
        GameObject.Find("Canvas/TextCount").GetComponent<Text>().text = "1";
        yield return new WaitForSecondsRealtime(1);
        GameObject.Find("Canvas/TextCount").GetComponent<Text>().text = "";
        Time.timeScale = 1;
    }
}
