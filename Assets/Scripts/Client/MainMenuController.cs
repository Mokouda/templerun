﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Assets.Scripts.Metier;
using Assets.Scripts.Physique;

public class MainMenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        //On récupère notre sauvegarde, ou on en créé une s'il n'y en a pas.
        if (PhysiqueFactory.getSaveSrv().GetByName("save") != null)
            MetierFactory.Save = PhysiqueFactory.getSaveSrv().GetByName("save");
        else
            MetierFactory.Save = PhysiqueFactory.getSaveSrv().Add(new Save());
        MetierFactory.Game = new Game(MetierFactory.Save.hardcore);
        //Lance la musique sauvegardée
        if (MetierFactory.Save.musicChosen == 0)
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/StartMenu");
        else
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/Undertale");
        GameObject.Find("Audio Source").GetComponent<AudioSource>().Play();
        //Charger la bonne skybox
        RenderSettings.skybox = Resources.Load<Material>("Skybox/skybox" + MetierFactory.Save.skyboxChosen);
        //Commencer avec le bon personnage!
        if (MetierFactory.Save.characterChosen == 0)
            GameObject.Find("PanelMain/CharacterOk").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UnityChanOk");
        else
            GameObject.Find("PanelMain/CharacterOk").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/SapphiChanOk");

        //Pour ma petite bétise avec le generic!
        GameObject.Find("Canvas").SetActive(!MetierFactory.Save.generic);
        GameObject.Find("CanvasGeneric").SetActive(MetierFactory.Save.generic);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("escape"))
            Quit();

    }

    public void Play()
    {
        SceneManager.LoadScene("Game");
    }


    public void ShowStats()
    {
        GameObject.Find("Canvas/PanelStats/TextScore").GetComponent<Text>().text = "best score : \n" + MetierFactory.Save.highScore;
        GameObject.Find("Canvas/PanelStats/TextMeter").GetComponent<Text>().text = "best distance : \n" + MetierFactory.Save.highMeter + " mètres";
        GameObject.Find("Canvas/PanelStats/TextRing").GetComponent<Text>().text = "max anneau : \n" + MetierFactory.Save.highRings;
    }

    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

}
