﻿using Assets.Scripts.Metier;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    public GameObject[] groundPrefabs;
    public GameObject[] ringPrefabs;
    public GameObject[] powerPrefabs;

    private Transform player;

    private float spawnZ = -22.5f;
    private float groundLenght = 7.5f;
    private float speedUp = 200;

    private int nbGroundOnScreen = 8;
    private int lastPrefabIndex = 0;
    private int powerIndex = 0;
    private bool beginning;

    private List<GameObject> activeGrounds;

	// Use this for initialization
	private void Start () {
        //On initialise une nouvelle partie.
        MetierFactory.Game = new Game(MetierFactory.Save.hardcore);
        if(MetierFactory.Save.musicChosen == 0)
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/Kirby");
        else
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/Megalovania");
        GameObject.Find("Audio Source").GetComponent<AudioSource>().Play();

        RenderSettings.skybox = Resources.Load<Material>("Skybox/skybox" + MetierFactory.Save.skyboxChosen);
        beginning = true;
        activeGrounds = new List<GameObject>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        powerIndex = Random.Range(0, 2);

        for (int i = 0; i  < nbGroundOnScreen; i++)
        {
            //Permet de faire apparaître des sols normaux au début
                SpawnGround(0);
        }
        beginning = false;
    }
	
	// Update is called once per frame
	private void Update () {

        // Test si le joueur a passé un peu plus que la longueur d'un sol et si la dernière plateforme n'est pas un virage
		if(player.position.z - groundLenght > (spawnZ - nbGroundOnScreen * groundLenght) && lastPrefabIndex < 6)
        {
            SpawnGround();
            if(activeGrounds.Count > nbGroundOnScreen)
                DeleteGround();
        }
        //Si on tourne
        else if (MetierFactory.Game.canTurn && !MetierFactory.Game.turnUnlock)
        {
            MetierFactory.Game.canTurn = false;
            MetierFactory.Game.turnUnlock = true;
            lastPrefabIndex = 3;

            for (int i = 0; i < activeGrounds.Count - 2; i++)
                DeleteGround();
            SpawnGround(0);
            SpawnGround(0);
        }

        if ((MetierFactory.Game.score / speedUp) >= 1)
            SpeedUp();

	}

    //Spawn un sol aléatoirement
    private void SpawnGround(int prefabIndex = -1)
    {
        float random = Random.Range(1, 100);
        GameObject go;
        //Permet obligatoirement d'avoir  un sol "normal" après un pont
        if (lastPrefabIndex == 3)
        {
            go = Instantiate(groundPrefabs[0]);
            lastPrefabIndex = 0;
        }
        else
        {
            //Permet de faire apparaître des sols normaux au début, sinon random
            if (prefabIndex == -1)
                go = Instantiate(groundPrefabs[RandomPrefabIndex()]);
            else
                go = Instantiate(groundPrefabs[prefabIndex]);
        }
        // Permet uniquement de rendre "les" sols enfants du GameObject GameController, c'est plus propre
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        //On conserve notre avancement dans l'axe Z
        spawnZ += groundLenght;
        //Donne une position aléatoire aux ponts
        if (lastPrefabIndex == 3)
        {
            go.transform.Translate(new Vector3(Random.Range(-1f, 1), 0, 0));
        }
        //Fait spawn des anneaux ou un power selon un random.
        if(random >= 75 && !beginning)
            ringSpawn(go);
        if (random <= 3 && !beginning)
            powerSpawn(go);
        //Ceci nous permettra de supprimer le dernier élément "sol"
        activeGrounds.Add(go);
    }

    private void powerSpawn(GameObject go)
    {
        GameObject power = Instantiate(powerPrefabs[powerIndex]);
        powerIndex++;
        if (powerIndex > 2)
            powerIndex = 0;
        power.transform.SetParent(go.transform);
        switch (go.name)
        {
            case "GroundHole(Clone)":
                power.transform.position = new Vector3(Random.Range(-1f, 1), 1.5f, go.transform.position.z - groundLenght / 2);
                break;
            case "GroundFencerLeft(Clone)":
                power.transform.position = new Vector3(1, 1.5f, go.transform.position.z - groundLenght / 2);
                break;
            case "GroundFencerRight(Clone)":
                power.transform.position = new Vector3(-1, 1.5f, go.transform.position.z - groundLenght / 2);
                break;
            case "Bridge(Clone)":
                power.transform.position = new Vector3(0, 1.5f, go.transform.position.z - groundLenght / 2);
                break;
            default:
                power.transform.position = new Vector3(Random.Range(-1f, 1), 1.5f, go.transform.position.z - groundLenght / 2);
                break;
        }
        power.transform.SetParent(null);
    }

    // Fait spawn les anneaux à un endroit donné selon le sol
    private void ringSpawn(GameObject  go)
    {
        GameObject ring;
        switch (go.name)
        {
            case "GroundHole(Clone)":
                ring = Instantiate(ringPrefabs[1]);
                ring.transform.SetParent(go.transform);
                ring.transform.position = new Vector3(Random.Range(-1f, 1), 0, go.transform.position.z - groundLenght);
                break;
            case "GroundFencerLeft(Clone)":
                ring = Instantiate(ringPrefabs[0]);
                ring.transform.SetParent(go.transform);
                ring.transform.position = new Vector3(1, 0, go.transform.position.z - groundLenght);
                break;
            case "GroundFencerRight(Clone)":
                ring = Instantiate(ringPrefabs[0]);
                ring.transform.SetParent(go.transform);
                ring.transform.position = new Vector3(-1, 0, go.transform.position.z - groundLenght);
                break;
            case "Bridge(Clone)":
                ring = Instantiate(ringPrefabs[0]);
                ring.transform.SetParent(go.transform);
                ring.transform.position = new Vector3(0, 0, go.transform.position.z - groundLenght);
                break;
            default:
                ring = Instantiate(ringPrefabs[0]);
                ring.transform.SetParent(go.transform);
                ring.transform.position = new Vector3(Random.Range(-1f, 1), 0, go.transform.position.z - groundLenght);
                break;
        }
    }

    //Delete le sol passé
    private void DeleteGround(int i = 0)
    {
        Destroy(activeGrounds[i]);
        activeGrounds.RemoveAt(i);
    }

    //Détermine le sol à spawn
    private int RandomPrefabIndex()
    {
        if (groundPrefabs.Length <= 1)
            return 0;

        int randomIndex = lastPrefabIndex;

        while(randomIndex == lastPrefabIndex)
        {
            randomIndex = Random.Range(0, groundPrefabs.Length);
        }

        lastPrefabIndex = randomIndex;

        return randomIndex;
    }

    private void SpeedUp()
    {
        MetierFactory.Game.speed += 2;
        speedUp *= 10;
        MetierFactory.Game.bonusRings++;
    }
}
