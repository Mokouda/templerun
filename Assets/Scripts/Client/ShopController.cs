﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Metier;
using Assets.Scripts.Physique;

public class ShopController : MonoBehaviour {

    [SerializeField]
    private GameObject PanelSkybox;
    [SerializeField]
    private GameObject PanelMusic;
    [SerializeField]
    private GameObject PanelCharacter;

    void Start()
    {
        updateInfo();
    }

    //Mets à jour les infos du shop
	public void updateInfo()
    {
        GameObject.Find("TextNbRings").GetComponent<Text>().text = MetierFactory.Save.rings.ToString();
        GameObject.Find("PanelBonusRing/TextBonusRingCost").GetComponent<Text>().text = "coût :\n" + (Math.Pow(10, 1 + MetierFactory.Save.bonusRings)).ToString();
        GameObject.Find("PanelBonusRing/ButtonBonusRing/Text").GetComponent<Text>().text = "x" + (1 + MetierFactory.Save.bonusRings).ToString();
        GameObject.Find("PanelBonusScore/TextBonusScoreCost").GetComponent<Text>().text = "coût :\n" + (Math.Pow(10, 1 +MetierFactory.Save.bonusScore)).ToString();
        GameObject.Find("PanelBonusScore/ButtonBonusScore/Text").GetComponent<Text>().text = "x" + (1 + MetierFactory.Save.bonusScore).ToString();
        if (MetierFactory.Save.hardcore)
            GameObject.Find("ButtonHardcore/Text").GetComponent<Text>().text = "help plz";
        else
            GameObject.Find("ButtonHardcore/Text").GetComponent<Text>().text = "t'es sûr?";
        PanelSkybox.SetActive(!MetierFactory.Save.skyboxUnlock);
        PanelMusic.SetActive(!MetierFactory.Save.musicUnlock);
        PanelCharacter.SetActive(!MetierFactory.Save.characterUnlock);
    }

    //Augmente le bonus anneaux
    public void ringUp()
    {
        if (MetierFactory.Save.rings >= (Math.Pow(10, 1 + MetierFactory.Save.bonusRings)))
        {
            MetierFactory.Save.rings -= (int)Math.Pow(10, 1 + MetierFactory.Save.bonusRings);
            MetierFactory.Save.bonusRings++;
            PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
            updateInfo();
        }
    }

    //Augmente le bonus score
    public void scoreUp()
    {
        if (MetierFactory.Save.rings >= (Math.Pow(10, 1 + MetierFactory.Save.bonusScore)))
        {
            MetierFactory.Save.rings -= (int)Math.Pow(10, 1 + MetierFactory.Save.bonusScore);
            MetierFactory.Save.bonusScore++;
            PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
            updateInfo();
        }
    }


    //Débloque le changement de skybox
    public void unlockSkybox()
    {
        if(MetierFactory.Save.rings >= 10000)
        {
            MetierFactory.Save.rings -= 10000;
            MetierFactory.Save.skyboxUnlock = true;
            PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
        }
        updateInfo();
    }

    //Débloque le changement de musique
    public void unlockMusic()
    {
        if (MetierFactory.Save.rings >= 10000)
        {
            MetierFactory.Save.rings -= 10000;
            MetierFactory.Save.musicUnlock = true;
            PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
        }
        updateInfo();
    }

    //Débloque le changement de musique
    public void unlockCharacter()
    {
        if (MetierFactory.Save.rings >= 20000)
        {
            MetierFactory.Save.rings -= 20000;
            MetierFactory.Save.characterUnlock = true;
            PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
        }
        updateInfo();
    }

    //Change de skybox
    public void skyboxChoose(int nb)
    {
        MetierFactory.Save.skyboxChosen = nb;
        RenderSettings.skybox = Resources.Load<Material>("Skybox/skybox"+ nb);
        PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
    }

    //Change de musique
    public void musicChoose(int nb)
    {
        MetierFactory.Save.musicChosen = nb;
        if (nb == 0)
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/StartMenu");
        else
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/Undertale");
        GameObject.Find("Audio Source").GetComponent<AudioSource>().Play();
        PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
    }

    //Change de Character
    public void characterChoose(int nb)
    {
        MetierFactory.Save.characterChosen = nb;
        if (nb == 0)
            GameObject.Find("PanelMain/CharacterOk").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UnityChanOk");
        else
            GameObject.Find("PanelMain/CharacterOk").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/SapphiChanOk");

        PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
    }

    //Débloque/bloque le mode hardcore, tout est doublé, même les anneaux/score.
    public void unleashHell()
    {
        MetierFactory.Save.hardcore = !MetierFactory.Save.hardcore;
        PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
        updateInfo();
    }
}
