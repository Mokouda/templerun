﻿using System.Collections;
using Assets.Scripts.Metier;
using Assets.Scripts.Physique;
using UnityEngine;

public class GenericController : MonoBehaviour {

    [SerializeField]
    private GameObject MainCanvas;
    [SerializeField]
    private AudioSource audioSource;
    private Vector3 positionInitiale;

    void Start()
    {
        itBegins();
    }


    public void itBegins () {
        StartCoroutine(End());
        positionInitiale = GameObject.Find("PanelGeneric").transform.position;
        audioSource.clip = Resources.Load<AudioClip>("Musics/StarWarsTheme");
        audioSource.Play();
        MetierFactory.Save.generic = false;
        PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
    }

    // Update is called once per frame
    void Update () {
        GameObject.Find("PanelGeneric").transform.Translate(new Vector3(0, 0.07f, 0));
    }

    private IEnumerator End()
    {
        yield return new WaitForSeconds(50f);
        Stop();
    }

    public void Stop()
    {
        GameObject.Find("PanelGeneric").transform.position = positionInitiale;
        audioSource.Stop();
        if (MetierFactory.Save.musicChosen == 0)
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/StartMenu");
        else
            GameObject.Find("Audio Source").GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("Musics/Undertale");
        GameObject.Find("Audio Source").GetComponent<AudioSource>().Play();
        MainCanvas.SetActive(true);
        gameObject.SetActive(false);
    }
}
