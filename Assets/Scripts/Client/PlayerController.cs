﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Metier;
using Assets.Scripts.Physique;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private GameObject PanelGameOver;

    private CharacterController character;
    private GameObject model3D;
    private Vector3 moveVector;
    private Vector2 direction;

    private float verticalVelocity = 0.0f;
    private float gravity = 14f;

    private float cameraY = 0.0f;
    private float cameraZ = 0.0f;

    private bool turnRestriction = false;

    private int life = 1;

    private GameObject camera;

    //Pour gérer le swipe:
    private float minDistY = 120;
    private float minDistX = 100;
    private Vector2 startPos;

	// Use this for initialization
	void Start () {
        character = GetComponent<CharacterController>();
        //On initialise notre model character selon notre choix, grâce à une architecture respectée sur les noms des animations, nous ne faisons aucun code en plus!
        if (MetierFactory.Save.characterChosen == 0)
            GameObject.Find("Player/CharacterSapphi").SetActive(false);
        else
            GameObject.Find("Player/CharacterUnity").SetActive(false);
        model3D = GameObject.FindGameObjectWithTag("Character");
        //On récupère les coordonnées de départ de la camera pour les utiliser dans l'update
        camera = GameObject.Find("Player/Main Camera");
        cameraY = camera.transform.position.y;
        cameraZ = camera.transform.position.z;

        model3D.GetComponent<Animator>().Play("RUN00_F");
	}
	
	// Update is called once per frame
	void Update () {

        if (MetierFactory.Game.gameOver)
            return;

        model3D.GetComponent<Animator>().speed = MetierFactory.Game.speed / 5;

        MetierFactory.Game.score += ((int)transform.position.z - MetierFactory.Game.meter) * (int)MetierFactory.Game.speed;
        MetierFactory.Game.meter = (int) transform.position.z;

        moveVector = Vector3.zero;
        //On vérifie si on est sur le sol ou non, sinon on tombe
        if (character.isGrounded)
            verticalVelocity = -0.2f;
        else
        {
            if (!model3D.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("JUMP00"))
                verticalVelocity -= gravity * Time.deltaTime * (MetierFactory.Game.speed / 5f);
            else
                verticalVelocity += 0.2f * Time.deltaTime;
        }

        //Code pour gérer les actions/déplacements sur portable.
        if (Input.touchCount > 0)
        {
        //Permet de faire une action uniquement pendant  qu'on court.
        if (model3D.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("RUN00_F"))
            {
            Touch touch = Input.touches[0];

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startPos = touch.position;
                    break;
                case TouchPhase.Stationary:
                        break;
                case TouchPhase.Ended:
                    float verticalDist = (new Vector3(0, touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;
                    if (verticalDist > minDistY)
                    {
                        float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
                        //Jump et glissage
                        if (swipeValue > 0)
                            StartCoroutine(Jump());
                        else if (swipeValue < 0)
                            StartCoroutine(Slide());
                    }

                    float horizontalDist = (new Vector3(touch.position.x, 0, 0) - new Vector3(startPos.x, 0, 0)).magnitude;

                    if (horizontalDist > minDistX)
                    {
                        float swipeValue = Mathf.Sign(touch.position.x - startPos.x);

                        //Tourner à droite ou à gauche
                        if (swipeValue > 0)
                            StartCoroutine(Turn(-5f));
                        else if (swipeValue < 0)
                            StartCoroutine(Turn(5f));
                    }
                    break;
            }
            //Code utilisé pour faire les test sur PC
            //On saute
            //if (Input.GetKeyDown("space"))
            //    StartCoroutine(Jump());
            //    //On Slide
            //      if (Input.GetKeyDown("p"))
            //    StartCoroutine(Slide());
            //    //Tourner à droite
            //    if (Input.GetKeyDown("y") && !turnRestriction && transform.position.y >= 0)
            //            StartCoroutine(Turn(-5f));
            //    //Tourner à gauche
            //    if (Input.GetKeyDown("u") && !turnRestriction && transform.position.y >= 0)
            //            StartCoroutine(Turn(5f));
            }
        }

        //Pour le portable
        moveVector.x = Input.acceleration.x * MetierFactory.Game.speed * 1.4f;
        //Pour le pc (avec les flèches)
        //moveVector.x = Input.GetAxisRaw("Horizontal") * MetierFactory.Game.speed;

        moveVector.y = verticalVelocity;

        moveVector.z = MetierFactory.Game.speed;

        if(transform.position.y < -2.5)
        {
            GameOver("Tu es tombé dans l'espace infini...\nMême si ce n'est pas logique vu l'absence de gravité..");
        }

        character.Move(moveVector * Time.deltaTime);

        //Grâce à cameraY et cameraZ on bloque la position x de la camera pour avoir le même effet que le Temple Run original
        if(!MetierFactory.Game.canTurn)
            camera.transform.position = new Vector3(0, transform.position.y + cameraY, transform.position.z + cameraZ);
    }

    // Change l'animation.
    private IEnumerator Jump()
    {
        model3D.GetComponent<Animator>().Play("JUMP00");
        yield return new WaitForSeconds(1.4f / (MetierFactory.Game.speed / 5));
        model3D.GetComponent<Animator>().Play("RUN00_F");
    }

    //Change l'animation et permet de traverser les obstacles de type "slide"
    private IEnumerator Slide()
    {
        model3D.GetComponent<Animator>().Play("SLIDE00");
        //Le character peut ignorer les collisions de types "ObstacleSlide" si il est en train de glisser
        if(GameObject.FindGameObjectWithTag("ObstacleSlide")!=null)
            Physics.IgnoreCollision(character, GameObject.FindGameObjectWithTag("ObstacleSlide").GetComponent<Collider>(), true);
        yield return new WaitForSeconds(1.4f / (MetierFactory.Game.speed / 5));
        model3D.GetComponent<Animator>().Play("RUN00_F");
        yield return new WaitForSeconds(0.6f);
        if (GameObject.FindGameObjectWithTag("ObstacleSlide") != null)
            Physics.IgnoreCollision(character, GameObject.FindGameObjectWithTag("ObstacleSlide").GetComponent<Collider>(), false);
        
        
    }

    //Tourne selon y : -90 à droite, 90 à gauche.
    private IEnumerator Turn(float y)
    {
        if (MetierFactory.Game.canTurn)
        {
            turnRestriction = true;
            // Ici on simule le virage du personnage, pour faire un vrai virage, et cela est effectivement possible, il faudrait travailler, en plus de l'axe z sur l'axe x.
            // Le personnage devra donc non pas se déplacer en z+, mais aussi en z-, x+ et x-. La génération du chemin aussi deviendra z-/z+/x-/x+ au lieu de z+
            // Je pense pouvoir le faire, mais par soucis de rapidité (et vu que je n'y avais pas pensé avant pour dire vrai...) je vous fournis cette version là.. Je pense aussi qu'elle est du coup plus optimisée.
            for (int i = 0; i < 18; i++)
            {
                transform.Rotate(0, -y, 0, Space.Self);
                yield return new WaitForSeconds(0.01f);
            }
            transform.Rotate(0, (y*18), 0, Space.Self);
            //On récupère le dernier GameObject "enfant" de GameController, ce sera donc un virage dans tout les cas.
            GameObject go = GameObject.Find("GameController").transform.GetChild(GameObject.Find("GameController").transform.childCount - 1).gameObject;
            if (go.name == "GroundTurnRight(Clone)" && y == -5)
            {
                MetierFactory.Game.turnUnlock = false;
                go.transform.Rotate(0, y * 18, 0, Space.Self);
            }
            else if(go.name == "GroundTurnLeft(Clone)" && y == 5)
            {
                MetierFactory.Game.turnUnlock = false;
                go.transform.Rotate(0, y * 18, 0, Space.Self);

            }  
            else if(go.name == "GroundTurn(Clone)")
            {
                MetierFactory.Game.turnUnlock = false;
                go.transform.Rotate(0, y * 18, 0, Space.Self);
            }
            this.life = 1;
        }
        else
        {
            this.life--;
            if (life < 0)
            {
                GameOver("A force de te cogner contre les colliders, tu as fini par mourir..");
            }
        }
        yield return new WaitForSeconds(1f / (MetierFactory.Game.speed / 5));
        turnRestriction = false;
    }

    //Lance une fonction selon le nom de l'object avec lequel on entre en collision
    private void OnControllerColliderHit(ControllerColliderHit collider)
    {
        MetierFactory.Game.canTurn = false;
        
        if (collider.point.z > transform.position.z)
        {
            switch (collider.gameObject.name)
            {
                case "Slide":
                    GameOver("Facepalm mortel..");
                    break;
                case "FencerRight":
                    GameOver("Aïe! Il fallait aller à droite!");
                    break;
                case "FencerLeft":
                    GameOver("Aïe! Il fallait aller à gauche!");
                    break;
                case "PalmTree1":
                    GameOver("Face-Palm! Mais cette fois, littéralement!");
                    break;
                case "FenceOfDeath":
                    if(!turnRestriction)
                        GameOver("Le principe d'un virage, c'est de tourner!");
                    break;
                case "Turn":
                    MetierFactory.Game.canTurn = true;
                    break;
            }

        }
        
                   
    }

    //Fin de la partie et sauvegarde des résultats
    private void GameOver(string deathSentence)
    {
        MetierFactory.Game.gameOver = true;

        if (transform.position.y > -2.5)
            model3D.GetComponent<Animator>().Play("DAMAGED01");
        PanelGameOver.SetActive(true);
        GameObject.Find("Canvas/PanelGameOver/TextDeathSentence").GetComponent<Text>().text = deathSentence;
        GameObject.Find("Canvas/PanelGameOver/TextScore").GetComponent<Text>().text = "Score : " + MetierFactory.Game.score;
        GameObject.Find("Canvas/PanelGameOver/TextRings").GetComponent<Text>().text = "Anneaux : " + MetierFactory.Game.rings;
        GameObject.Find("Canvas/PanelGameOver/TextMeter").GetComponent<Text>().text = "Mètres : " + MetierFactory.Game.meter;

        if(MetierFactory.Save.characterChosen == 0)
            GameObject.Find("Canvas/PanelGameOver/Image/CharacterNo").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/UnityChanNo");
        else
            GameObject.Find("Canvas/PanelGameOver/Image/CharacterNo").GetComponent<Image>().sprite = Resources.Load<Sprite>("Sprites/SapphiChanNo");

        saveUpdate();
        PhysiqueFactory.getSaveSrv().Update(MetierFactory.Save);
    }

    private void saveUpdate()
    {
        MetierFactory.Save.rings += MetierFactory.Game.rings;
        if (MetierFactory.Save.highMeter < MetierFactory.Game.meter)
        {
            GameObject.Find("Canvas/PanelGameOver/TextMeter").GetComponent<Text>().text = "Mètres (New): " + MetierFactory.Game.meter;
            MetierFactory.Save.highMeter = MetierFactory.Game.meter;
        }
        if (MetierFactory.Save.highScore < MetierFactory.Game.score)
        {
            GameObject.Find("Canvas/PanelGameOver/TextScore").GetComponent<Text>().text = "Score (New): " + MetierFactory.Game.score;
            MetierFactory.Save.highScore = MetierFactory.Game.score;
        }
        if (MetierFactory.Save.highRings < MetierFactory.Game.rings)
            MetierFactory.Save.highRings = MetierFactory.Game.rings;


    }

    //Les "speed/5" permettent de modifier les animations de façon dynamique selon la vitesse du joueur.
}
