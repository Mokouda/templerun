﻿using System.Collections;
using UnityEngine;
using Assets.Scripts.Metier;

public class PowerController : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0, 1, 0, Space.Self);
    }

    void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.tag == "Character")
        {
            GetComponent<BoxCollider>().enabled = false;
            
            switch (gameObject.name)
            {
                case "magnetPower(Clone)":
                    GetComponent<SpriteRenderer>().enabled = false;
                    StartCoroutine(magnetPower());
                    break;
                case "ringPower(Clone)":
                    GetComponent<MeshRenderer>().enabled = false;
                    StartCoroutine(ringPower());
                    break;
                case "scorePower(Clone)":
                    GetComponent<MeshRenderer>().enabled = false;
                    StartCoroutine(scorePower());
                    break;
            }
            Destroy(gameObject, 21);
        }
    }

    private IEnumerator magnetPower()
    {
        MetierFactory.Game.magnetPower = true;
        //Rajoute un effet de lumière sur le personnage... Juste pour le swag.
        GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().enabled = true;
        GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().color = Color.blue;
        yield return new WaitForSeconds(20f);
        GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().enabled = false;
        MetierFactory.Game.magnetPower = false;
    }

    private IEnumerator ringPower()
    {
        MetierFactory.Game.bonusRings *= 2;
        GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().enabled = true;
        GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().color = Color.yellow;
        yield return new WaitForSeconds(20f);
        if (!MetierFactory.Game.gameOver)
        {
            MetierFactory.Game.bonusRings /= 2;
            GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().enabled = false;
        }
    }

    private IEnumerator scorePower()
    {
        MetierFactory.Game.bonusScore *= 2;
        GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().enabled = true;
        GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().color = Color.red;
        yield return new WaitForSeconds(20f);
        if (!MetierFactory.Game.gameOver)
        {
            MetierFactory.Game.bonusScore /= 2;
            GameObject.FindGameObjectWithTag("Character").GetComponent<Light>().enabled = true;
        }
            
    }

}
