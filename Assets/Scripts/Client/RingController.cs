﻿using UnityEngine;
using Assets.Scripts.Metier;

public class RingController : MonoBehaviour {
	
    private Vector3 v = Vector3.zero;

    void Start()
    {
        transform.eulerAngles = v;
    }

	// Update is called once per frame
	void Update () {
        v.y += 1;
        transform.eulerAngles = v;

        if (MetierFactory.Game.magnetPower)
        {
            if ((transform.parent.transform.position.z - GameObject.FindGameObjectWithTag("Character").transform.position.z) < 2)
                transform.position = Vector3.MoveTowards(transform.position, GameObject.FindGameObjectWithTag("Character").transform.position, Time.deltaTime * MetierFactory.Game.speed);
        }
    }

    //Lancé lorsque l'object, avec lequel le script est attaché, entre en collision.
    void OnCollisionEnter(Collision collider)
    {
        if(collider.gameObject.tag == "Character")
        {
            GetComponent<BoxCollider>().enabled = false;
            GetComponentInChildren<MeshRenderer>().enabled = false;
            AudioSource source = GetComponent<AudioSource>();
            source.Play();
            Destroy(gameObject, 1);
            MetierFactory.Game.rings += MetierFactory.Save.bonusRings * MetierFactory.Game.bonusRings;
            MetierFactory.Game.score += (MetierFactory.Save.bonusRings * MetierFactory.Game.bonusRings) * 10;
            if (MetierFactory.Save.hardcore)
            {
                MetierFactory.Game.rings += MetierFactory.Save.bonusRings * MetierFactory.Game.bonusRings;
                MetierFactory.Game.score += (MetierFactory.Save.bonusRings * MetierFactory.Game.bonusRings) * 10;
            }  
        }
    }
}
