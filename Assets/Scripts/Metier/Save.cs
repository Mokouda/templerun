﻿using System;
namespace Assets.Scripts.Metier
{
    [Serializable]
    public class Save
    {

        public string name { get; set; }
        public int highMeter { get; set; }
        public int highScore { get; set; }
        public int highRings { get; set; }
        public int rings { get; set; }
        public int bonusRings { get; set; }
        public int bonusScore { get; set; }
        public int skyboxChosen { get; set; }
        public int musicChosen { get; set; }
        public int characterChosen { get; set; }

        public bool generic { get; set; }
        public bool skyboxUnlock { get; set; }
        public bool musicUnlock { get; set; }
        public bool characterUnlock { get; set; }
        public bool hardcore { get; set; }

        //Ici, pour avoir un système de succès, il suffit de rajouter plusieurs bool qui seront débloqué via un SuccesController, comme par exemple faire 10000 Score pour la première fois etc...


        public Save()
        {
            this.name = "save";
            this.rings = 0;
            this.highMeter = 0;
            this.highScore = 0;
            this.highRings = 0;
            this.bonusRings = 1;
            this.bonusScore = 1;
            this.skyboxChosen = 1;
            this.musicChosen = 0;
            this.characterChosen = 0;
            this.generic = true;
            this.skyboxUnlock = false;
            this.musicUnlock = false;
            this.characterUnlock = false;
            this.hardcore = false;
        }
    }
}