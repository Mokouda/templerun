﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game {

    public float speed { get; set; }
    public int score { get; set; }
    public int rings { get; set; }
    public int meter { get; set; }
    public int bonusScore { get; set; }
    public int bonusRings { get; set; }
    public bool magnetPower { get; set; }
    public bool canTurn { get; set; }
    //A faux, il permet de ne pas tourner une nouvelle fois trop vite
    public bool turnUnlock { get; set; }
    public bool gameOver { get; set; }

    public Game(bool hardcore)
    {
        if (!hardcore)
            this.speed = 7;
        else
            this.speed = 14;
        this.score = 0;
        this.rings = 0;
        this.meter = 0;
        this.bonusScore = 1;
        this.bonusRings = 1;
        this.magnetPower = false;
        this.turnUnlock = true;
        this.canTurn = true;
        this.gameOver = false;
    }
}
