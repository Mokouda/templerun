﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace Assets.Scripts.Metier
{
    public class SerializationHelper
    {
        private static bool _byPassEncrypt = false;

        public static string Serialize(object obj)
        {
            using (MemoryStream stream = new MemoryStream())
            using (StreamWriter writer = new StreamWriter(stream, Encoding.UTF8))
            {
                XmlSerializer xs = new XmlSerializer(obj.GetType());
                xs.Serialize(writer, obj);
                return Encoding.UTF8.GetString(stream.ToArray());
            }
        }

        public static object Deserialize(string serialized, Type type)
        {
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(serialized)))
            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
            {
                XmlSerializer xs = new XmlSerializer(type);
                return xs.Deserialize(stream);
            }
        }

        public static string Encrypt(string toEncrypt)
        {
            if (_byPassEncrypt)
            {
                return toEncrypt;
            }
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes("12345678901234567890123456789012");
            // 256-AES key
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.PKCS7;
            // better lang support
            ICryptoTransform cTransform = rDel.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string toDecrypt)
        {
            if (_byPassEncrypt)
            {
                return toDecrypt;
            }
            byte[] keyArray = UTF8Encoding.UTF8.GetBytes("12345678901234567890123456789012");
            // AES-256 key
            byte[] toEncryptArray = Convert.FromBase64String(toDecrypt);
            RijndaelManaged rDel = new RijndaelManaged();
            rDel.Key = keyArray;
            rDel.Mode = CipherMode.ECB;
            // http://msdn.microsoft.com/en-us/library/system.security.cryptography.ciphermode.aspx
            rDel.Padding = PaddingMode.PKCS7;
            // better lang support
            ICryptoTransform cTransform = rDel.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
    }
}

