﻿using System.Collections.Generic;
using Assets.Scripts.Metier;

namespace Assets.Scripts.Physique
{
    public interface ISaveService
    {
        Save Add(Save s);
        void Remove(Save s);
        Save Update(Save updated);
        List<Save> GetAll();
        int GetCount();
        Save GetByName(string name);
    }
}
