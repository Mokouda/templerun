﻿using System;
using System.IO;
using UnityEngine;

namespace Assets.Scripts.Physique
{
    public class PhysiqueFactory
    {

        private static ISaveService saveSrv = new SaveServiceImpl();
        private static string _savePath;

        public static ISaveService getSaveSrv()
        {
            return saveSrv;
        }


        public static string GetSavePath()
        {
            if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
            {
                _savePath = PlayerPrefs.GetString("SavePath");
                if (_savePath == "")
                {
                    _savePath = "/sdcard/MJTempleRun/Saves/";
                    _savePath = _savePath.Replace('\\', '/');
                    PlayerPrefs.SetString("SavePath", _savePath);
                    PlayerPrefs.Save();
                }
                return _savePath;
            }
            else if (Application.platform == RuntimePlatform.WindowsEditor)
            {
                _savePath = PlayerPrefs.GetString("SavePathEditor");
                if (_savePath == "")
                {
                    _savePath = "../MJTempleRun/Assets/Resources/Saves/";
                    PlayerPrefs.SetString("SavePathEditor", _savePath);
                    PlayerPrefs.Save();
                }
                return _savePath;
            }
            else if (Application.platform == RuntimePlatform.WindowsPlayer)
            {
                _savePath = PlayerPrefs.GetString("SavePath");
                if (_savePath == "")
                {
                    _savePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/Jeux/MJTempleRun/Saves/";
                    _savePath = _savePath.Replace('\\', '/');
                    PlayerPrefs.SetString("SavePath", _savePath);
                    PlayerPrefs.Save();
                }
                return _savePath;
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public static void SetSavePath(string savePath)
        {
            if (savePath != _savePath)
            {
                savePath = savePath.Replace('\\', '/');
                if (!Directory.Exists(savePath))
                {
                    Directory.CreateDirectory(savePath);
                }
                savePath += "Saves/";
                Directory.Move(_savePath, savePath);
                _savePath = savePath;
                if (Application.platform == RuntimePlatform.WindowsEditor)
                {
                    PlayerPrefs.SetString("SavePathEditor", _savePath);
                }
                else
                {
                    PlayerPrefs.SetString("SavePath", _savePath);
                }
                PlayerPrefs.Save();
            }
        }
    }
}
