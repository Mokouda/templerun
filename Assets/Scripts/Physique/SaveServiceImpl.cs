﻿using Assets.Scripts.Metier;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System;

namespace Assets.Scripts.Physique
{
    class SaveServiceImpl : ISaveService
    {
        private string _SavePath = "/Saves/";

        public SaveServiceImpl()
        {
            Directory.CreateDirectory(PhysiqueFactory.GetSavePath() + _SavePath);
        }

        public Save Add(Save s)
        {
            try
            {
                string str = SerializationHelper.Encrypt(SerializationHelper.Serialize(s));
                File.WriteAllText(PhysiqueFactory.GetSavePath() + _SavePath + s.name + ".xml", str);
            }
            catch
            {
                Console.WriteLine("Impossible d'ajouter la sauvegarde suivante : " + s.ToString());
                return null;
            }
            return s;
        }

        public List<Save> GetAll()
        {
            StringBuilder sb;
            List<Save> saves = new List<Save>();
            Save save = new Save();
            try
            {
                foreach (string fileName in Directory.GetFiles(PhysiqueFactory.GetSavePath() + _SavePath, "*.xml").Select(Path.GetFileName))
                {
                    sb = new StringBuilder();
                    foreach (string line in File.ReadAllLines(PhysiqueFactory.GetSavePath() + _SavePath + fileName))
                    {
                        sb.Append(line);
                    }
                    save = (Save)SerializationHelper.Deserialize(SerializationHelper.Decrypt(sb.ToString()), typeof(Save));
                    saves.Add(save);
                }
            }
            catch
            {
                Console.WriteLine("Impossible de récupérer la liste de sauvegarde.\nL'ajout s'est arrêté à la sauvegarde suivante : " + save.ToString());
                return null;
            }
            return saves;
        }

        public Save GetByName(string name)
        {
            StringBuilder sb = new StringBuilder();
            Save save = new Save();
            try
            {
                foreach (string line in File.ReadAllLines(PhysiqueFactory.GetSavePath() + _SavePath + name + ".xml"))
                {
                    sb.Append(line);
                }
                save = (Save)SerializationHelper.Deserialize(SerializationHelper.Decrypt(sb.ToString()), typeof(Save));
            }
            catch
            {
                Console.WriteLine("Impossible de récupérer la sauvegarde avec le nom : " + name);
                return null;
            }
            return save;
        }

        public int GetCount()
        {
            return Directory.GetFiles(PhysiqueFactory.GetSavePath() + _SavePath, "*.xml").Length;
        }

        public void Remove(Save s)
        {
            try
            {
                File.Delete(PhysiqueFactory.GetSavePath() + _SavePath + s.name + ".xml");
            }
            catch
            {
                Console.WriteLine("Impossible de supprimer la sauvegarde suivante : " + s.ToString());
            }
        }

        public Save Update(Save updatedSave)
        {
            try
            {
                Remove(GetByName("save"));
                Add(updatedSave);
            }
            catch
            {
                Console.WriteLine("Impossible de modifier la sauvegarde suivante : " + GetByName("save").ToString());
                return null;
            }
            return updatedSave;
        }
    }
}